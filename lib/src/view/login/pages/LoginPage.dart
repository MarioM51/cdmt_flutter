import 'package:flutter/material.dart';

import 'package:cdmt_flutter_backoffice/src/view/login/widgets/LoginForm.dart';


class LoginPage extends StatelessWidget {

  static const NAME = "login";

  const LoginPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final _screenSize = MediaQuery.of(context).size;
    final double allHeight = _screenSize.height;
    final double allWidth = _screenSize.width;

    return Scaffold(
      body: LoginForm(width: allWidth*0.85, marginTop: allHeight*0.3 ),
    );

  }
}