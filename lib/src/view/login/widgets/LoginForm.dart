import 'package:flutter/material.dart';

import 'package:cdmt_flutter_backoffice/src/controller/LoginBLoC.dart';
import 'package:cdmt_flutter_backoffice/src/controller/user/IUserController.dart';
import 'package:cdmt_flutter_backoffice/src/controller/user/impl/UserControllerImpl.dart';
import 'package:cdmt_flutter_backoffice/src/view/product/pages/ProductsPage.dart';
import '../../ControllerViewConnector.dart';


class LoginForm extends StatefulWidget {

  final double width;
  final double marginTop;


  const LoginForm({Key key, @required double width, @required double marginTop})
      : this.width=width, this.marginTop = marginTop, super(key: key) ;

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {

    final LoginBLoC loginBLoC = ControllerViewConnector.loginCtrlOf(context);

    final boxDecoration = BoxDecoration(
      //border: Border.all(color: Colors.black),
      //color: Colors.white,
      //borderRadius: BorderRadius.circular(5),
      boxShadow: [
        BoxShadow(
          //color: Colors.grey,
          offset: Offset(0.0, 0.5), //(x,y)
          blurRadius: 1.0,
        ),
      ],
    );

    final inputEmailBuilder = StreamBuilder(
      stream: loginBLoC.getEmailStream(),
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          margin: EdgeInsets.only(top: 20),
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: TextField(
            onChanged: (txt) { 
              loginBLoC.getChangeEmail()(txt);
            },
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              icon: Icon(Icons.alternate_email),
              //counterText: snapshot.data,
              hintText: 'mario@mail.com',
              labelText: 'Correo electronico',
              errorText: snapshot.error,
              
            ),
          ),
        );
      },
    );

    final passInput = StreamBuilder(
      stream: loginBLoC.getPassStream(),
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          margin: EdgeInsets.only(top: 20),
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: TextField(
            onChanged: (input) {
              loginBLoC.getChangePass()(input);
            },
            keyboardType: TextInputType.visiblePassword,
            obscureText: true,

            obscuringCharacter: '*',
            decoration: InputDecoration(
              icon: Icon(Icons.lock),
              //counterText: snapshot.data,
              labelText: 'Contraseña',
              errorText: snapshot.error,
            ),
          ),
        );
      },
    );

    final submitButton =  StreamBuilder(
      stream: loginBLoC.getFormValid(),
      //initialData: false,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)
            ),
            //color: Colors.deepPurple,
            //textColor: Colors.white,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80, vertical: 15),
              child: (!_isLoading) ? Text('Ingresar') : Container( width: 15, height: 15, child: CircularProgressIndicator(strokeWidth: 2) ),
            ),
            // iniciar con el boton deshabilitado por defecto deshabilitado al inicio
            onPressed: !snapshot.hasData ? null : () { _login(context, loginBLoC); },
          ),
        );
      },
    );

    final form = Container(
      width: widget.width,
      padding: EdgeInsets.symmetric(vertical: 50),
      decoration: boxDecoration,
      child: Column(
        children: [
          Text('Login', style: TextStyle(fontSize: 20),),
          inputEmailBuilder,
          passInput,
          SizedBox(height: 30),
          submitButton,
        ],
      ),
    );

    return SingleChildScrollView(
      child: Center(
        child: Column(
          children: [
            SizedBox(height: widget.marginTop),//separacion con el top
            form,
            //SizedBox(height: 20), //separacion entre el recuperar pass
            //Text('Recuperar contraseña'),
            SizedBox(height: 100), // separacion con el bottom cuando esta en modo horizontal
          ],
        ),
      ),
    );
  }

  void _login(BuildContext context, LoginBLoC loginBLoC) async {
    final IUserController userApi = new UserControllerImpl();

    this._isLoading = true;
    setState(() {});
    userApi.auth(loginBLoC.email, loginBLoC.pass)
      .then((bool authSuccess) {
        if(authSuccess) {
          Navigator.of(context).pushReplacementNamed(ProductsPage.NAME);
        } else {
          loginBLoC.addPassError("Usuario o contraseña incorrectos");
          this._isLoading = false;
          setState(() {});
        }
      })
    ;
  }
}