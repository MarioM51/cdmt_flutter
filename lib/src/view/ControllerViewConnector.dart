import 'package:cdmt_flutter_backoffice/src/controller/LoginBLoC.dart';
import 'package:cdmt_flutter_backoffice/src/controller/product/IProductController.dart';
import 'package:cdmt_flutter_backoffice/src/controller/product/impl/ProductContollerImpl.dart';
import 'package:flutter/material.dart';


class ControllerViewConnector extends InheritedWidget {

  static ControllerViewConnector _singleton;

  factory ControllerViewConnector({ Key key, Widget child }) {
    if(_singleton == null) {
      _singleton = new ControllerViewConnector._internal(key: key, child: child);
    }
    return _singleton;
  }

  ControllerViewConnector._internal({ Key key, Widget child })
      : super(key: key, child: child);



  final loginBLoC = new LoginBLoC();
  final IProductController _productController = new ProductContollerImpl();

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) {
    return true;
  }

  static LoginBLoC loginCtrlOf (BuildContext ctx) {
    return (
      ctx.dependOnInheritedWidgetOfExactType<ControllerViewConnector>().loginBLoC
    );
  }

  static IProductController productCtrlOf (BuildContext ctx) {
    return (
      ctx.dependOnInheritedWidgetOfExactType<ControllerViewConnector>()._productController
    );
  }
}