import 'dart:io';

import 'package:cdmt_flutter_backoffice/src/controller/product/dto/ProductImageDTO.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:cdmt_flutter_backoffice/src/view/utils/Constants.dart';

class ImagePickerField extends StatefulWidget {
  
  final Function(File) _onChange;

  final List<ProductImageDTO> images;
  
  ImagePickerField({@required Function(File) onChange, List<ProductImageDTO> images, Key key}) :
    this._onChange=onChange,
    this.images = images,
    super(key: key)
  ;

  @override
  _ImagePickerFieldState createState() => _ImagePickerFieldState();
}

class _ImagePickerFieldState extends State<ImagePickerField> {

  File _file;

  bool downloadImage = false;

  @override
  void initState() { 
    this.downloadImage = (widget.images != null && widget.images.length > 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //final _random = Random();

    int idMainImage;
    if(this.downloadImage) {
      idMainImage = widget.images.where((i) => i.mainImage == true ).first.productImageId;
    }

    // TODO: Agregar y cambiar a obtener imagen por el productImageId, y no del productId
    final String url = 'http://192.168.1.76:8080/api/products/images/$idMainImage';
    

    final productImage = (this.downloadImage)
      ? FadeInImage(
          placeholder: AssetImage(Constants.LOADING_IMAGE),
          image: NetworkImage(url),
        )
      : Image(
          image: AssetImage( _file?.path ?? Constants.NO_IMAGE),
          height: 200,
          fit: BoxFit.cover,
        )
    ;

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              alignment: AlignmentDirectional.topStart,
              margin: EdgeInsets.only(left: 20),
              child: Text('Imagen: '),
            ),
            Row(
              children: [
                IconButton( icon: Icon(Icons.photo_size_select_actual, color: Theme.of(context).buttonColor ),
                  onPressed: () { _pickPhoto(ImageSource.gallery); },
                ),
                IconButton( icon: Icon(Icons.camera_alt, color: Theme.of(context).buttonColor),
                  onPressed: () { _pickPhoto(ImageSource.camera); },
                )
              ],
            ),
          ],
        ),
        productImage
      ],
    );

  }

  void _pickPhoto(ImageSource imageSource) async {
    final picker = ImagePicker();
    PickedFile imagePicker = await picker.getImage(
      source: imageSource,
      imageQuality: 10,
      //maxWidth: 240,
      //maxHeight: 135,
    );
    if(imagePicker?.path != null) {
      _file = File(imagePicker?.path) ?? null;
    }
    if (_file != null) {
      widget._onChange(_file);
    }
    this.downloadImage = false;
    setState(() { });
  }
}