
enum InternalMessageType {
  PRODUCT_ADDED
}

class InternalMessage {
  final InternalMessageType type;
  final dynamic body;

  InternalMessage(this.type, this.body);

}