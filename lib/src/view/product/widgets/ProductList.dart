import 'package:cdmt_flutter_backoffice/src/controller/product/IProductController.dart';
import 'package:flutter/material.dart';

import 'package:cdmt_flutter_backoffice/src/controller/product/dto/ProductDetailsDTO.dart';

class ProductList extends StatelessWidget {
  final IProductController productCtrl;
  final Function(BuildContext context, ProductDetailsDTO product) onTap;

  const ProductList(
    IProductController productCtrl,
    Function(BuildContext context,
    ProductDetailsDTO product) onTapItem, {Key key}
  ) :
    this.productCtrl = productCtrl,
    this.onTap = onTapItem,
    super(key: key)
  ;

  @override
  Widget build(BuildContext context) {

    final productList = StreamBuilder(
      stream: productCtrl.getStream(),
      builder: (BuildContext context, AsyncSnapshot<List<ProductDetailsDTO>> snapshot){
        if (snapshot.hasData) {
            return (snapshot.data.length > 0)
                ?
                  Container( padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: ListView.builder(
                      itemCount: snapshot.data.length ,
                      itemBuilder: (ctx, i) {
                        return _buildProductItem(snapshot.data[i], i, context);
                      }
                    ),
                  )
                :
                  Center(
                    child: Text('Lista de productos vacia'),
                  )
              // (snapshot.data.length > 0)
            ;
        }
        if (snapshot.hasError) {
          return Center( child: Text(snapshot.error) );
        } else{ 
          return Center( child: CircularProgressIndicator(), );
        }
        
        
          
      },
    );

    return Container(
      child: productList,
    );
  }

  Widget _buildProductItem(ProductDetailsDTO product, int index, BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: Text(product.name),
          subtitle: Text('Precio: ${product.price}\nCantidad: ${product.amount}' ),
          trailing: Text(product.status, style: TextStyle(fontSize: 10),),
          onTap: () => { onTap(context, product) },
        ),
        Divider()
      ],
    );
  }

}