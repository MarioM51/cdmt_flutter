import 'dart:io';
import 'package:cdmt_flutter_backoffice/src/view/utils/widgets/ImagePickerWidget.dart';
import 'package:flutter/material.dart';


import 'package:cdmt_flutter_backoffice/src/controller/product/IProductController.dart';
import 'package:cdmt_flutter_backoffice/src/controller/product/dto/ProductDetailsDTO.dart';

import '../../ControllerViewConnector.dart';

//typedef OnSubmitProduct = Future<void> Function(ProductDetailsDTO newProduct, BuildContext context);

class ProductForm extends StatefulWidget {
  
  final ProductDetailsDTO product;

  
  ProductForm(ProductDetailsDTO product, {Key key}) :
    this.product = product,
    super(key: key)
  ;

  @override
  _ProductFormState createState() => _ProductFormState();
}

class _ProductFormState extends State<ProductForm> {

  File _image;
  
  final formKey = GlobalKey<FormState>();

  String errorMsg;

  @override
  Widget build(BuildContext context) {
    
    final inputName = Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: TextFormField(
        initialValue: widget.product.name,
        onChanged: (value) { widget.product.name = value; },
        validator: (_) { return widget.product.nameError(); },
        maxLength: 60,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          labelText: 'Nombre',
        ),
      ),
    );

    final inputPrice = Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: TextFormField(
        initialValue: (widget.product.price != null) ? '${widget.product.price}' : '',
        onChanged: (value) { widget.product.price = double.parse(value); },
        validator: (_) { return widget.product.priceError(); } ,
        keyboardType: TextInputType.number,
        maxLength: 9,
        decoration: InputDecoration(
          labelText: 'Precio',
        ),
      ),
    );

    final inputAmount = Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: TextFormField(
        initialValue: (widget.product.amount != null) ? '${widget.product.amount}' : '',
        onChanged: (value) { widget.product.amount = int.parse(value); },
        validator: (_) { return widget.product.amountError(); } ,
        maxLength: 9,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Cantidad',
        ),
      ),
    );
    
    final inputDescription = Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: TextFormField(
        initialValue: widget.product.description,
        onChanged: (value) { widget.product.description = value; },
        validator: (_) { return widget.product.descriptionError(); } ,
        keyboardType: TextInputType.text,
        maxLines: 5,
        maxLength: 160,
        decoration: InputDecoration(
          labelText: 'Descripcion',
        ),
      ),
    );

    bool isUpdateForm = (widget.product.productId==null || widget.product.productId <= 0);
    final IProductController productCtrl = ControllerViewConnector.productCtrlOf(context);

    final submitButtom = StreamBuilder(
      stream: productCtrl.getStream(),
      builder: (BuildContext context, AsyncSnapshot<List<ProductDetailsDTO>> snapshot){
        if (snapshot.hasData) {
            return RaisedButton(
              shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(5.0) ),
              onPressed: (true) 
                ? () => { this.onSubmit(context) }
                : null,
              child: (isUpdateForm) ? Text('Agregar') : Text('Actualizar')
            );
        } else{ 
          return Center( child: CircularProgressIndicator(), );
        }  
      },
    );
    
    return Form(
      key: formKey,
      child: Column(
        children: [
          inputName,
          inputPrice,
          ImagePickerField(
            images: widget.product.images,
            onChange: (File image) => {
              this._image = image
            },
          ),
          inputAmount, 
          //switchAvailable,
          inputDescription,
          SizedBox(height: 10,),
          (this.errorMsg != null) 
            ? Text(this.errorMsg, style: TextStyle(color: Colors.red, fontSize: 10))
            : Container(),
          submitButtom,
          SizedBox(height: 20,)
        ],
      )
    );
  }

  void onSubmit(BuildContext context) {
    bool formIsValid = formKey.currentState.validate();
    // "setState" usado para consultar el isLoaging del controller
    setState(() { });
    if(formIsValid) {  
      this.doSubmit(context)
        .then((_) => Navigator.of(context).pop())
        .catchError((error) {
          this.errorMsg = error;
          setState(() { });
        })
      ;
    }
    setState(() { });
  }

  Future<void> doSubmit(BuildContext context) async {
    bool isAddForm =  widget.product.productId == null || widget.product.productId <= 0;
    final IProductController productCtrl = ControllerViewConnector.productCtrlOf(context);
    if(isAddForm) {
      await productCtrl.save(widget.product, _image);
    } else {
      await productCtrl.update(widget.product, _image);
    }
  }

}