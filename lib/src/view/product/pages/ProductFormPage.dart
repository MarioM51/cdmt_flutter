import 'package:cdmt_flutter_backoffice/src/controller/product/IProductController.dart';
import 'package:flutter/material.dart';

import 'package:cdmt_flutter_backoffice/src/controller/product/dto/ProductDetailsDTO.dart';

import 'package:cdmt_flutter_backoffice/src/view/product/widgets/ProductForm.dart';

import '../../ControllerViewConnector.dart';

class ProductFormPage extends StatelessWidget {

  static const NAME = 'product-form';

  const ProductFormPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    ProductDetailsDTO product = ModalRoute.of(context).settings.arguments;
    if(product == null) {
      product = new ProductDetailsDTO();
    }

    final appBar = AppBar(
      title: Text('Productos'),
      actions: [
        (product.productId != null)
        ? IconButton(
            icon: Icon(Icons.delete, color: Colors.red),
            onPressed: () { deleteProduct(context, product); },
          )
        : Container()
      ],
    );

    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        child: ProductForm(product)
      )
    );
  }

  void deleteProduct(BuildContext context, ProductDetailsDTO product) {
    _mostrarConfirmcion(context).then((positiveConfirm) {
      if(positiveConfirm) {
        final IProductController productCtrl = ControllerViewConnector.productCtrlOf(context);
        productCtrl.deleteById(product);
        Navigator.of(context).pop();
      }
    });
  }

  Future<bool> _mostrarConfirmcion(BuildContext context) async {
   return showDialog(
      context: context,
      barrierDismissible: true, // cerrar al pulsar afuera de este
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          title: Text('Eliminar'),
          content: Column(
            mainAxisSize: MainAxisSize.min, // evitar ue tome todo lo disponible, asi solo tomara lo necesario
            children: [
              Text('¿Esta seguro que quiere eliminar el producto?')
            ],
          ),
          actions: [
            FlatButton(
              color: Colors.red,
              child: Text('Eliminar'),
              onPressed: (){ Navigator.of(context).pop(true); },
            ),
            FlatButton(
              child: Text('Cancelar'),
              onPressed: (){ Navigator.of(context).pop(false); }, // accion de regresar
            ),
          ],
        );
      }
    );
  }


}

