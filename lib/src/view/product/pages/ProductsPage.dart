
import 'package:cdmt_flutter_backoffice/src/controller/product/IProductController.dart';
import 'package:cdmt_flutter_backoffice/src/controller/product/dto/ProductDetailsDTO.dart';
import 'package:cdmt_flutter_backoffice/src/view/product/widgets/ProductList.dart';
import 'package:flutter/material.dart';


import '../../ControllerViewConnector.dart';
import 'ProductFormPage.dart';

class ProductsPage extends StatelessWidget {

  static const NAME = "products";

  ProductsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final IProductController productCtrl = ControllerViewConnector.productCtrlOf(context);
    productCtrl.findAll();

    return Scaffold(
      appBar: AppBar(
        title: Text('Productos'),
      ),
      body: ProductList(productCtrl, _goToProductForm),
      floatingActionButton: FloatingActionButton(
        onPressed: () => { _goToProductForm(context, null) },
        child: const Icon(Icons.add),
      ),
    );
  }

  void _goToProductForm(BuildContext context, ProductDetailsDTO product) {
    Navigator.of(context).pushNamed(ProductFormPage.NAME, arguments: product);
  }

}