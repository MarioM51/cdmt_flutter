import 'package:flutter/material.dart';


import 'view/ControllerViewConnector.dart';
import 'view/login/pages/LoginPage.dart';
import './view/product/pages/ProductsPage.dart';
import './view/product/pages/ProductFormPage.dart';


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ControllerViewConnector(
      child: MaterialApp(
        title: 'CDMT',
        initialRoute: LoginPage.NAME,
        routes: {
          ProductsPage.NAME : (ctx) => ProductsPage(),
          LoginPage.NAME : (ctx) => LoginPage(),
          ProductFormPage.NAME : (ctx) => ProductFormPage(),
        },
        debugShowCheckedModeBanner: false,
        theme: ThemeData.dark().copyWith(
          accentColor: Colors.white,
        )
          
      ),
    );
  }
}
