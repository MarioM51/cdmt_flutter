import 'dart:convert';

import './../../utils/AbstractServerMapper.dart';
import 'ProductImageMapper.dart';

class ProductDetailsMapper extends AbstractServerMapper {

  int productId;
  String name;
  int amount;
  double price;
  String description;
  String status;
  DateTime createAt;
  DateTime lastUpdate;
  List<ProductImageMapper> images;

  ProductDetailsMapper({ this.productId, this.name, this.amount, this.price, this.description, this.status, this.createAt, this.lastUpdate, this.images });
  
  @override
  ProductDetailsMapper jsonStringToObject(String jsonString) {
    final jsonDecoded = json.decode(jsonString);

    ProductImageMapper productImageMapper = new ProductImageMapper();

    final imagesString = json.encode(jsonDecoded["images"]);
    List<ProductImageMapper> imagesList;
    if(imagesString != null && imagesString != "null") {
      final List itemList = productImageMapper.jsonStringToObjecttList(imagesString);
      imagesList = itemList.cast<ProductImageMapper>();
    } else {
      imagesList = new List<ProductImageMapper>();
    }
    
    var product = ProductDetailsMapper(
      productId: jsonDecoded["productId"],
      name: jsonDecoded["name"],
      amount: jsonDecoded["amount"],
      price: jsonDecoded["price"],
      description: jsonDecoded["description"],
      status: jsonDecoded["status"],
      createAt: (jsonDecoded["createAt"] != null) ? DateTime.parse(jsonDecoded["createAt"]) : null,
      lastUpdate: (jsonDecoded["createAt"] != null) ? DateTime.parse(jsonDecoded["lastUpdate"]) : null,
      images: imagesList
    );

    return product;
  }
  
  @override
  String objectToJsonString() {
    //ProductImageMapper productImageMapper = new ProductImageMapper();
    Map<String, dynamic> map = {
      "productId": productId,
      "name": name,
      "amount": amount,
      "price": price,
      "description": description,
      "status": status,
      "createAt": createAt.toString(),
      "lastUpdate": lastUpdate.toString(),
      //"images": productImageMapper.objectToJsonStringList(images),
    };
    return json.encode(map);
  }

  @override
  String toString() {
    return 'productId: $productId, name: $name, amount: $amount, price: $price, description: $description, status: $status, createAt: $createAt, lastUpdate: $lastUpdate,';
  }

}
