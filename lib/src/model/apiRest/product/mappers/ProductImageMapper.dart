import 'dart:convert';

import 'package:cdmt_flutter_backoffice/src/model/apiRest/utils/AbstractServerMapper.dart';

class ProductImageMapper extends AbstractServerMapper {
  int productImageId;
  String imageName;
  String contentType;
  bool mainImage;
  
  ProductImageMapper({
      this.productImageId,
      this.imageName,
      this.contentType,
      this.mainImage,
  });

  @override
  AbstractServerMapper jsonStringToObject(String jsonString) {
    final jsonMapped = json.decode(jsonString);
    final product = ProductImageMapper(
        productImageId: jsonMapped["productImageId"],
        imageName: jsonMapped["imageName"],
        contentType: jsonMapped["contentType"],
        mainImage: jsonMapped["mainImage"],
    );
    return product;

  }

  @override
  String objectToJsonString() {
    final Map<String, dynamic> jsonMap = {
        "productImageId": this.productImageId,
        "imageName": this.imageName,
        "contentType": this.contentType,
        "mainImage": this.mainImage,
    };
    String jsonString = json.encode(jsonMap);
    throw jsonString;
  }
}
