import 'dart:io';

import 'package:cdmt_flutter_backoffice/src/model/apiRest/product/mappers/ProductDetailsMapper.dart';



abstract class IProductDaoApi {

  Future<ProductDetailsMapper> save(ProductDetailsMapper newProduct);

  Future<List<ProductDetailsMapper>> findAll();

  Future<ProductDetailsMapper> update(ProductDetailsMapper newProduct);

  Future<Object> uploadImageProduct(int productId, File image);

  Future<ProductDetailsMapper> delete(ProductDetailsMapper toDeleteMapper);

}