import 'dart:convert';
import 'dart:io';

import 'package:cdmt_flutter_backoffice/src/model/apiRest/product/mappers/ProductImageMapper.dart';

import './../mappers/ProductDetailsMapper.dart';
import './../../utils/AbstractRequestHander.dart';
import './../../utils/RequestDataSet.dart';
import './../IProductDaoApi.dart';

class ProductDaoApiImpl extends AbstractRequestHander implements IProductDaoApi {

  static const _URL = '${AbstractRequestHander.DOMAIN}/products';

  static const Map<String, String> REQUEST_HEADERS = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
  };

  Future<ProductDetailsMapper> save(ProductDetailsMapper newProduct) async {
    RequestDataSet newProductRequestData = RequestDataSet(
      httpMethod: HttpMethod.POST,
      url: _URL,
      jsonBody: newProduct.objectToJsonString(),
      headers: REQUEST_HEADERS,
    );
    ProductDetailsMapper productSaved = await super.request(newProductRequestData, newProduct.jsonStringToObject);
    return productSaved;
  }

  Future<List<ProductDetailsMapper>> findAll() async {
    RequestDataSet findAllRequestData = RequestDataSet(
      httpMethod: HttpMethod.GET,
      url: _URL,
      headers: REQUEST_HEADERS,
    );
    List allMapper = await super.request(
      findAllRequestData, new ProductDetailsMapper().jsonStringToObjecttList
    );

    List<ProductDetailsMapper> allProductsFinded = allMapper.cast<ProductDetailsMapper>();

    return allProductsFinded;
    
  }

  @override
  Future<ProductDetailsMapper> update(ProductDetailsMapper newInfoProduct) async {
    assert(newInfoProduct.productId != null);

    newInfoProduct.images = null;

    RequestDataSet findAllRequestData = RequestDataSet(
      httpMethod: HttpMethod.PUT,
      url: _URL,
      jsonBody: newInfoProduct.objectToJsonString(),
      headers: REQUEST_HEADERS,
    );

    ProductDetailsMapper productUpdated = await super.request(
      findAllRequestData, new ProductDetailsMapper().jsonStringToObject
    );

    return productUpdated;

  }

  @override
  Future<ProductImageMapper> uploadImageProduct(int productId, File image) async {
    String endPoint = '$_URL/$productId/images';
    final Map resp = await super.uploadImage(endPoint, image);
    ProductImageMapper productImageMapper = ProductImageMapper();
    productImageMapper = productImageMapper.jsonStringToObject(json.encode(resp));
    return productImageMapper;
  }

  @override
  Future<ProductDetailsMapper> delete(ProductDetailsMapper toDelete) async {
    final endpoint = '$_URL/${toDelete.productId}';
    
    RequestDataSet deleteRequestData = RequestDataSet(
      httpMethod: HttpMethod.DELETE,
      url: endpoint,
      headers: REQUEST_HEADERS,
    );

    ProductDetailsMapper productDeleted = await super.request(
      deleteRequestData, new ProductDetailsMapper().jsonStringToObject
    );

    return productDeleted;

  }

}