

import 'package:flutter/cupertino.dart';

enum HttpMethod {
  GET,
  POST,
  PUT,
  DELETE    
}

class RequestDataSet {
  final HttpMethod _httpMethod;
  final String _url;
  final String _jsonBody;
  final Map<String, String> _headers;
  
  RequestDataSet({ @required HttpMethod httpMethod, @required String url, String jsonBody, Map<String, String> headers }) :
    this._httpMethod = httpMethod,
    this._url = url,
    this._jsonBody = jsonBody,
    this._headers = headers
  ;

  get httpMethod => _httpMethod;
  get url => _url;
  get jsonBody => _jsonBody;
  get headers => _headers;

  @override
  String toString() {
    return 'Method: $httpMethod, Url: $url, Headers: $headers, Body: $jsonBody';
  }
  
}