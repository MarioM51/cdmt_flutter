import 'dart:convert';

abstract class AbstractServerMapper {
  String objectToJsonString();

  String objectToJsonStringList(List<AbstractServerMapper> data) {
    return json.encode(List<AbstractServerMapper>.from(data.map((x) => x.objectToJsonString())));
  }

  AbstractServerMapper jsonStringToObject(String jsonString);

  List<AbstractServerMapper> jsonStringToObjecttList(String str) {
    List<dynamic> jsonListMap = json.decode(str);
    
    final list = List<AbstractServerMapper>();

    jsonListMap.forEach( (x) {
      String jsonStringItem = json.encode(x);
      list.add(this.jsonStringToObject(jsonStringItem));
    });

    return list;
  }

}