import 'dart:async' show TimeoutException;
import 'dart:convert';
import 'dart:io' show File, SocketException;

import 'package:cdmt_flutter_backoffice/src/model/apiRest/errorHandler/exceptions/ErrorMapper.dart';
import 'package:http/http.dart' as http;
import 'package:mime_type/mime_type.dart' as mime;
import 'package:http_parser/http_parser.dart' show MediaType;

import 'RequestDataSet.dart';

typedef Future<http.Response> Request();

abstract class AbstractRequestHander  {

  static const DOMAIN = 'http://192.168.1.76:8080/api';
  static final encoding = Encoding.getByName('utf-8');
  static const TIME_OUT = 10;

  Future<Object> request(RequestDataSet requestData, Function(String) stringToObject ) async {
    final http.Response response = await _doRequest(requestData);
    await Future.delayed(Duration(seconds: 2), ()=>{});
    final statusCode = response.statusCode;
    final isOkResponse = (statusCode == 201 || response.statusCode == 200 || response.statusCode == 202);
    final String respString = _encode(response);
    
    if(isOkResponse) {
      final responseMapped = stringToObject(respString);
      return responseMapped;
    } else {
      this._handleError(respString, statusCode);
      return null;
    }
  }

  Future<http.Response> _doRequest(RequestDataSet requestData) async {
    print('===========================');
    print('Request: $requestData');

    const timeOutDuration = const Duration(seconds: TIME_OUT);
    http.Response response;
    try {
      switch (requestData.httpMethod) {
        case HttpMethod.GET:
          response = await http.get(requestData.url, headers: requestData.headers)
            .timeout(timeOutDuration);
        break;

        case HttpMethod.POST:
          response = await http.post(requestData.url, body: requestData.jsonBody, headers: requestData.headers,
          encoding: encoding)
            .timeout(timeOutDuration);
        break;

        case HttpMethod.PUT:
          response = await http.put(requestData.url, body: requestData.jsonBody, headers: requestData.headers, encoding: encoding)
            .timeout(timeOutDuration);
        break;

        case HttpMethod.DELETE:
          response = await http.delete(requestData.url, headers: requestData.headers)
          .timeout(timeOutDuration);
        break;

        default:
          throw "Metodo ${requestData.httpMethod} no definido";
      }
    } catch(ex) {
      if(ex is SocketException || ex is TimeoutException) {
        throw 'Servidor tardo en responder, intente mas tarde';
      }
      throw 'Error, intente mas terde';
    }
    
    print('Response: Status: ${response.statusCode}, Body: ${response.body}');
    print('===========================');
    return response;

  }

  void _handleError(String errorBody, int statusCode) {
    ErrorMapper errorMapper = new ErrorMapper();
    errorMapper = errorMapper.jsonStringToObject(errorBody);
    if(errorMapper.errorType == "BAD_INPUT") {
      throw errorMapper.details[0];
    }
  }

  Future<Map> uploadImage(String endpoint, File image) async {
    final fileName = 'image';
    final httpMethod = 'POST';
    final Uri url = Uri.parse(endpoint);
    print('=========================== UPLOAD FILE');
    print('Request: $httpMethod, Url: $url, FileName: $fileName');

    final List<String> mimeType = mime.mime(image.path).split('/');
    final type = MediaType(mimeType[0], mimeType[1]); // ejemplo: "image/jpeg"

    final file  = await http.MultipartFile.fromPath(fileName, image.path, contentType: type);
    
    final uploadRequest = http.MultipartRequest(httpMethod, url);
    uploadRequest.files.add(file);

    final streamResponse = await uploadRequest.send();

    final resp = await http.Response.fromStream(streamResponse);
    final responseString = json.decode(resp.body);

    print('Response: Status: ${resp.statusCode}, BodyResp: $responseString');
    print('===========================');

    if(resp.statusCode == 200 || resp.statusCode == 201) {
      return responseString;
    } else {
      print('=============-ERROR UPLIADING FILE-==============');
      return null;
    }
  }

  String _encode(http.Response response) {
    return utf8.decode(response.bodyBytes);
  }

}