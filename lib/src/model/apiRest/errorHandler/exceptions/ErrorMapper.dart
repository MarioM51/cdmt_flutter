import 'dart:convert';

import 'package:cdmt_flutter_backoffice/src/model/apiRest/utils/AbstractServerMapper.dart';

class ErrorMapper extends AbstractServerMapper {

  List<String> details;
  String message;
  String errorType;
  
  ErrorMapper({
      this.details,
      this.message,
      this.errorType,
  });

  //factory ErrorMapper.fromJson(Map<String, dynamic> json) => 

  Map<String, dynamic> toJson() => {
      "details": List<dynamic>.from(details.map((x) => x)),
      "message": message,
      "errorType": errorType,
  };

  @override
  ErrorMapper jsonStringToObject(String jsonString) {
    final Map mapJson = json.decode(jsonString);
    final object = ErrorMapper(
      details: List<String>.from(mapJson["details"].map((x) => x)),
      message: mapJson["message"],
      errorType: mapJson["errorType"],
    );

    return object;
  }

  @override
  String objectToJsonString() {
    Map<String, dynamic> map = {
      "details": details,
      "message": message,
      "errorType": errorType,
    };
    String jsonString = json.encode(map);
    return jsonString;
  }

}
