abstract class IUserDaoApi {

  Future<bool> auth(String email, String pass);
  
}