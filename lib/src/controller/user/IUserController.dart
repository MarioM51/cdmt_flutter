abstract class IUserController {
  Future<bool> auth(String email, String pass);
}