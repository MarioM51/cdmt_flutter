import 'package:cdmt_flutter_backoffice/src/model/apiRest/user/IUserDaoApi.dart';
import 'package:cdmt_flutter_backoffice/src/model/apiRest/user/impl/UserDaoApiImpl.dart';

import '../IUserController.dart';

class UserControllerImpl implements IUserController {
  IUserDaoApi _userApi;

  UserControllerImpl() {
    _userApi = new UserDaoApiImpl();
  }
  
  @override
  Future<bool> auth(String email, String pass) async {
    return await _userApi.auth(email, pass);
  }

}