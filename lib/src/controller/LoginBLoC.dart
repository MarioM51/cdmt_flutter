import 'dart:async';


import 'package:rxdart/rxdart.dart';

import 'LoginValitator.dart';

//class LoginBLoC with Valitators {
class LoginBLoC with LoginValitator {

  final _emailController = BehaviorSubject<String>();
  final _passController = BehaviorSubject<String>();

  String get email => _emailController.value;
  String get pass => _passController.value;

  Stream<bool> getFormValid() {
    return Rx.combineLatest2(
      this.getEmailStream(),    // primer stream a combinar
      this.getPassStream(),     // segundo stream a combinar
      (e, p) { return true; }   // callback cuando ambos stream sean validos, recordar que cuando esta mal
                                // el valitator no pasa datos por el stream, solo cuando es valido
    );
  }

//creamos "getters/setters" para obtener la funcion para agregar datos al stream
// y asi evitarnos escribir lo de abajo en capas superiores
  Function(String) getChangeEmail() {
    return _emailController.sink.add;
  }

  Function(String) getChangePass() {
    return _passController.sink.add;
  }

  void addPassError(String error) {
    _passController.sink.addError(error);
  }

//Creamo "getters/setters" para obtener los screams, y asi poder obtenerlos
//para escuchar los datos que pasen por este, y asi le podremos agregar logica
//que se ejecute cada vez que cambie el input
  Stream<String> getEmailStream() {
    return _emailController.stream
      .transform(validateEmail) // variable "validatePass" la traemos del "Valitators"
    ;
  }

  Stream<String> getPassStream() {
    return _passController.stream
      .transform(validatePass)
    ;
  }

// cerramos los inputs
//los "?" es para evitarlos llamarlos si son null
  void dispose() {
    _emailController?.close();
    _passController?.close();
  }

}

/*
static LoginBloc of ( BuildContext context ) {
  return ( context.inheritFromWidgetOfExactType(Provider) as Provider ).loginBloc;
}


static LoginBloc of ( BuildContext context ){
   return context.dependOnInheritedWidgetOfExactType<Provider>().loginBloc;
}
*/