
import 'package:cdmt_flutter_backoffice/src/controller/product/dto/ProductDetailsDTO.dart';
import 'package:rxdart/rxdart.dart';


//class LoginBLoC with Valitators {
class ProductContollerViewHelper {

  final _productSubject = BehaviorSubject<List<ProductDetailsDTO>>();
  final _products = new List<ProductDetailsDTO>();

  final _loadingSubject = BehaviorSubject<bool>();

  ProductContollerViewHelper() {
    _productSubject.value = _products;
    _loadingSubject.value = false;
  }

  //data mng
  void addAll(List<ProductDetailsDTO> productsFinded) {
    _products.clear();
    _products.addAll(productsFinded);
    _updateUIList();
  }

  void add(ProductDetailsDTO newProduct) {
    _products.add(newProduct);
    _updateUIList();
  }

  void deleteById(int productId) {
    _products.removeWhere((p) => p.productId == productId );
    _updateUIList();
  }

  void update(ProductDetailsDTO product) {
    deleteById(product.productId);
    add(product);
  }

  // ui mng
  void _updateUIList() {
    _productSubject.sink.add(_products);
  }

  void setError(String error) {
    _productSubject.sink.addError(error);
  }

  Stream<List<ProductDetailsDTO>> getProductListStream() {
    return _productSubject.stream;
  }

  Stream<List<ProductDetailsDTO>> getListStream() {
    return _productSubject.stream;
  }

  void setLoading(bool loading) {
    if(loading) {
      _productSubject.sink.add(null);
    } else {
      _productSubject.sink.add(_products);
    }
    
  }

  void dispose() {
    _productSubject?.close();
    _loadingSubject?.close();
  }

}