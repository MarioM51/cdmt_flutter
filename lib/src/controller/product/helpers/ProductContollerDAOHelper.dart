import 'dart:io';

import 'package:cdmt_flutter_backoffice/src/controller/product/dto/ProductDetailsDTO.dart';
import 'package:cdmt_flutter_backoffice/src/controller/product/dto/ProductImageDTO.dart';
import 'package:cdmt_flutter_backoffice/src/controller/product/utils/ProductConverter.dart';
import 'package:cdmt_flutter_backoffice/src/model/apiRest/product/IProductDaoApi.dart';
import 'package:cdmt_flutter_backoffice/src/model/apiRest/product/impl/ProductDaoApiImpl.dart';
import 'package:cdmt_flutter_backoffice/src/model/apiRest/product/mappers/ProductDetailsMapper.dart';
import 'package:cdmt_flutter_backoffice/src/model/apiRest/product/mappers/ProductImageMapper.dart';

class ProductContollerDAOHelper {

  IProductDaoApi _productDaoApi;
  ProductConverter _productConverter;
  
  ProductContollerDAOHelper() {
    _productDaoApi = new ProductDaoApiImpl();
    _productConverter = new ProductConverter();
  }

  Future<ProductDetailsDTO> save(ProductDetailsDTO newProductUI, File image) async {
    //guardar
    ProductDetailsMapper productToSave = _productConverter.toMapper(newProductUI);
    ProductDetailsMapper productSaved = await _productDaoApi.save(productToSave);

    ProductDetailsDTO productToShow = _productConverter.toDTO(productSaved);

    //guardar imagen
    if(image != null) {
      ProductImageMapper productImageSaved = await _productDaoApi.uploadImageProduct(productSaved.productId, image);
      ProductImageDTO imageSavedToShow = _productConverter.imageToDTO(productImageSaved);
      productToShow.images.add(imageSavedToShow);
    }

    return productToShow;
  }

  Future<List<ProductDetailsDTO>> findAll() async {
    List<ProductDetailsMapper> productsFindedMappers = await _productDaoApi.findAll(); //throws TimeoutException
    List<ProductDetailsDTO> productsFindedDTOs = _productConverter.toDTOList(productsFindedMappers);
    return productsFindedDTOs;
  }


  Future<ProductDetailsDTO> update(ProductDetailsDTO newInfoProductFromUi, File image) async {
    ProductDetailsMapper newInfoProductToSend = _productConverter.toMapper(newInfoProductFromUi);
    ProductDetailsMapper productUpdated = await _productDaoApi.update(newInfoProductToSend);
    ProductDetailsDTO productUpdatedToShow = _productConverter.toDTO(productUpdated);
    
    bool updateImage = (image != null);
    if(updateImage) {
      ProductImageMapper productImageUpdated = await _productDaoApi.uploadImageProduct(newInfoProductFromUi.productId, image);
      ProductImageDTO productImageUpdatedToShow = _productConverter.imageToDTO(productImageUpdated);
      productUpdatedToShow.images.removeWhere((i) => i.mainImage == true);
      productUpdatedToShow.images.add(productImageUpdatedToShow);
    }
    
    return productUpdatedToShow;
  }

  Future<ProductDetailsDTO> delete(ProductDetailsDTO toDelete) async {
    final toDeleteMapper = _productConverter.toMapper(toDelete);
    ProductDetailsMapper productDelted = await _productDaoApi.delete(toDeleteMapper);
    ProductDetailsDTO productDeltedDto = _productConverter.toDTO(productDelted);
    return productDeltedDto;
  }

  

}