import 'package:cdmt_flutter_backoffice/src/controller/product/utils/UserMessages.dart';

import 'ProductImageDTO.dart';

class ProductDetailsDTO {

  int productId;
  String name;
  int amount;
  double price;
  String description;
  String status;

  List<ProductImageDTO> images;

  ProductDetailsDTO({ this.productId, this.name, this.amount, this.price, this.description, this.status, this.images});

  String nameError() {
    if(name == null || name.isEmpty) { return UserMessages.REQUIRED; }
    if(name.length <= 3 || name.length >= 160 ) { return UserMessages.NAME_BAD_LENGTH; }
    return null;
  }

  String amountError() {
    if(amount == null) { amount = 0; }
    if(amount == null || amount < 0) { return UserMessages.BIGGER_THAN_ZERO; }
    return null;
  }

  String priceError() {
    if(this.price == null ) { return UserMessages.REQUIRED; }
    if(price == null) { price = this.price; }
    if(price <= 0) { return UserMessages.BIGGER_THAN_ZERO; }
    return null;
  }

  String descriptionError() {
    if(this.description == null || description.isEmpty ) { return UserMessages.REQUIRED; }
    if(description.length <= 10 || description.length > 160) {
      return UserMessages.DESCRIPTION_BAD_LENGTH;
    }
    return null;
  }

  @override
  String toString() {
    return 'productId: $productId, name: $name, amount: $amount, price: $price, description: $description, status: $status';
  }

}
