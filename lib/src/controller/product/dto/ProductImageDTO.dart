
class ProductImageDTO {
  int productImageId;
  String imageName;
  String contentType;
  bool mainImage;
  
  ProductImageDTO({
      this.productImageId,
      this.imageName,
      this.contentType,
      this.mainImage,
  });

}
