import 'dart:async' show TimeoutException;
import 'dart:io';

import 'package:cdmt_flutter_backoffice/src/controller/product/IProductController.dart';
import 'package:cdmt_flutter_backoffice/src/controller/product/dto/ProductDetailsDTO.dart';
import 'package:cdmt_flutter_backoffice/src/controller/product/helpers/ProductContollerViewHelper.dart';
import 'package:cdmt_flutter_backoffice/src/controller/product/helpers/ProductContollerDAOHelper.dart';

class ProductContollerImpl implements IProductController {

  static const _TIME_OUT_MESSAGE = 'Servidor fuera de tiempo, intente mas tarde';

  ProductContollerDAOHelper _daoHelper;
  ProductContollerViewHelper _viewHelper;

  ProductContollerImpl() {
    _daoHelper = new ProductContollerDAOHelper();
    _viewHelper = new ProductContollerViewHelper();
  }

  @override
  Future<void> save(ProductDetailsDTO newProductUI, File image) async {
    try {
      _viewHelper.setLoading(true);
      ProductDetailsDTO productSaved = await _daoHelper.save(newProductUI, image);
      _viewHelper.add(productSaved);
      _viewHelper.setLoading(false);
    } catch(er) {
      if( er is String) {
        throw er;
      }
      print(er);
    } 

  }

  @override
  Future<void> findAll() async {
    try {
      _viewHelper.setLoading(true);
      List<ProductDetailsDTO> allProductsFinded = await _daoHelper.findAll();
      _viewHelper.addAll(allProductsFinded);
      _viewHelper.setLoading(false);
    } on TimeoutException catch(_) {
      _viewHelper.setError(_TIME_OUT_MESSAGE);
    }
  }

  @override
  Future<void> update(final ProductDetailsDTO newInfoProductFromUi, File image) async {
    _viewHelper.setLoading(true);
    ProductDetailsDTO productUpdatedToShow = await _daoHelper.update(newInfoProductFromUi, image);
    _viewHelper.update(productUpdatedToShow);
    _viewHelper.setLoading(false);
  }

  @override
  Stream<List<ProductDetailsDTO>> getStream() {
    return _viewHelper.getListStream();
  }

  @override
  Future<void> deleteById(ProductDetailsDTO toDelete) async {
    _daoHelper.delete(toDelete);
    _viewHelper.deleteById(toDelete.productId);
  }

}