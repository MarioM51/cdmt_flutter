class UserMessages {
  static const NAME_BAD_LENGTH = "El nombre debe estar entre 3 y 60 caracteres";

  static const BIGGER_THAN_ZERO = "Debe ser mayor a cero";

  static const DESCRIPTION_BAD_LENGTH = "La descripcion debe estar entre 10 y 160 caracteres";

  static const REQUIRED = "El campo es requerido";
}