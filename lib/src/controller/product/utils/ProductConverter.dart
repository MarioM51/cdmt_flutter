import 'package:cdmt_flutter_backoffice/src/controller/product/dto/ProductDetailsDTO.dart';
import 'package:cdmt_flutter_backoffice/src/controller/product/dto/ProductImageDTO.dart';
import 'package:cdmt_flutter_backoffice/src/model/apiRest/product/mappers/ProductDetailsMapper.dart';
import 'package:cdmt_flutter_backoffice/src/model/apiRest/product/mappers/ProductImageMapper.dart';

class ProductConverter {

  ProductDetailsDTO toDTO(ProductDetailsMapper mapper) {
    
    List<ProductImageDTO> images = new List<ProductImageDTO>();
    mapper.images.forEach((i) => {
      images.add(this.imageToDTO(i))
    });

    return new ProductDetailsDTO(
      name: mapper.name,
      price: mapper.price,
      amount: mapper.amount,
      description: mapper.description,
      productId: mapper.productId,
      status: mapper.status,
      images: images
    );
  }

  List<ProductDetailsDTO> toDTOList(List<ProductDetailsMapper> mappers) {
    var productList = List<ProductDetailsDTO>();
    mappers.forEach((p) => { productList.add(this.toDTO(p)) } );
    return productList;
  }


  ProductDetailsMapper toMapper(ProductDetailsDTO dto) {
    return new ProductDetailsMapper(
      name: dto.name,
      price: dto.price,
      amount: dto.amount,
      description: dto.description,
      productId: dto.productId,
      status: dto.status,
      //images: dto.images.cast<ProductImageMapper>()
    );
  }

  List<ProductDetailsMapper> toMapperList(List<ProductDetailsDTO> mappers) {
    var productList = List<ProductDetailsMapper>();
    mappers.forEach((p) => { productList.add(this.toMapper(p)) } );
    return productList;
  }

  ProductImageDTO imageToDTO(ProductImageMapper mapper) {
    final dto = new ProductImageDTO(
      productImageId: mapper.productImageId,
      contentType: mapper.contentType,
      imageName: mapper.imageName,
      mainImage: mapper.mainImage
    );
    return dto;
  }


}