import 'dart:io';

import 'package:cdmt_flutter_backoffice/src/controller/product/dto/ProductDetailsDTO.dart';

abstract class IProductController {

  Future<void> save(ProductDetailsDTO newProduct, File file);

  Future<void> update(ProductDetailsDTO newinfoProduct, File file);

  Future<void> findAll();

  Future<void> deleteById(ProductDetailsDTO newinfoProduct);

  Stream<List<ProductDetailsDTO>> getStream();

}