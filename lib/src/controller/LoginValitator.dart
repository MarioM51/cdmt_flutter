import 'dart:async';

class LoginValitator {
  
  final validatePass = StreamTransformer
    <String, //tipo de dato que entra
    String> //tipo de dato que sale
    .fromHandlers( 
      // "pass" es la varialbe que entra del stream, y el sink nos indica si el stream esta activo o y para
      // poder notificar que algo fallo en este
      handleData: (String pass, EventSink<String> sink) {
        if(pass.length <= 5) {
          sink.add(null);
          sink.addError('Debe tener mas de 6 caracteres');
        } else {
          //significa que el flujo es correcto
          sink.add(pass);
        }
      }
    )
  ;

  final validateEmail = StreamTransformer<String, String>
    .fromHandlers(
      handleData: (String email, EventSink<String> sink) {
        Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
        RegExp regExp = new RegExp(pattern);
        if(regExp.hasMatch(email)) {
          sink.add(email);
        } else {
          sink.add(null);
          sink.addError('Verifique formato');
        }
      }
    )
  ;

}
